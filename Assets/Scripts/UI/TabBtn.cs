using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

[RequireComponent(typeof(Image))]
public class TabBtn : MonoBehaviour, IPointerEnterHandler, IPointerClickHandler, IPointerExitHandler
{
    public Color green, orange;
    public Text statusTxt;

    [HideInInspector] public TabGroup tabGroup;
    [HideInInspector] public Image backGround;
    [HideInInspector] public GunSO gunSO;

    [SerializeField] Image image;

    // triggered when mouse Click
    public void OnPointerClick(PointerEventData eventData)
    {
        tabGroup.OntabSelected(this);
    }

    // triggered when mouse Enter
    public void OnPointerEnter(PointerEventData eventData)
    {
        tabGroup.OntabEnter(this);

    }

    // triggered when mouse Exit
    public void OnPointerExit(PointerEventData eventData)
    {
        tabGroup.OntabExit();

    }

    private void Start()
    {
        tabGroup.Subscribe(this);

        backGround = GetComponent<Image>();

        image.sprite = gunSO.gunSprite;

    }

    public GunSO GetGunSO()
    {
        return gunSO;
    }

    public void Select()
    {
        
    }
    public void Deselect()
    {

    }
}
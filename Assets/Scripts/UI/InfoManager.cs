using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InfoManager : MonoBehaviour
{
    [SerializeField] Text nameGunTxt, dmgTxt, DispersionTxt, rateOfFireTxt, reloadSpeedTxt, amumunationTxt
        , canUpgrade, yellowBtnTex, GreenBtnTex;

    [SerializeField] Button yellowBtn, greenBtn;

    GunSO gunSO;

    private void Awake()
    {
        yellowBtn.onClick.AddListener(() =>
        {
            AddValueToGunStat(10f);
            UpdateInfoDisplay(gunSO);
        });

        greenBtn.onClick.AddListener(() =>
        {
            AddValueToGunStat(-10f);
            UpdateInfoDisplay(gunSO);
        });
    }

    private void Start()
    {
        TabGroup.Instance.OnSelectTabBtn += TabGroup_OnSelectTabBtn;
    }

    private void TabGroup_OnSelectTabBtn(object sender, TabGroup.OnSelectTabBtnEventArgs e)
    {
        gunSO = e.gunSO;
        UpdateInfoDisplay(gunSO);

    }

    // Update display in info when data changed
    void UpdateInfoDisplay(GunSO gunSO)
    {
        nameGunTxt.text = gunSO.gunName;

        dmgTxt.text = gunSO.gunDmg.ToString();
        DispersionTxt.text = gunSO.gunDispersion.ToString();
        rateOfFireTxt.text = gunSO.gunFOR.ToString() + " RPM";
        reloadSpeedTxt.text = gunSO.gunReloadSpeed.ToString() + "%";
        amumunationTxt.text = gunSO.gunAmmunitio.ToString() + "/" + gunSO.gunAmmunitio.ToString();

        if (gunSO.upgrade)
        {
            canUpgrade.enabled = true;
        }

        yellowBtnTex.text = gunSO.yellowNum.ToString() + " BNB";
        GreenBtnTex.text = gunSO.greenNum.ToString() + " EWAR";
    }

    // Change value of gun
    void AddValueToGunStat(float value)
    {
        gunSO.gunDmg += value;
        gunSO.gunDispersion += value;
        gunSO.gunFOR += value;
        gunSO.gunReloadSpeed += value;
        gunSO.gunAmmunitio += value;
    }
}

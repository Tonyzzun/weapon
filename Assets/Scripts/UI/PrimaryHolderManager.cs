using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class PrimaryHolderManager : MonoBehaviour
{
    [SerializeField] Image primaryWeaponImage;
    [SerializeField] Text useTxt, rentTxt;
    [SerializeField] Button useBtn, rentOutBtn;

    [SerializeField] ConfirmationWindowUI confirmationWindowUI;

    GunSO gunSO;

    public event EventHandler OnClickUseBtn;
    public event EventHandler OnClickRentOutBtn;

    bool isUse;

    private void Awake()
    {
        useBtn.onClick.AddListener(() =>
        {
            if (gunSO.rentOut)
            {
                // Open confirm window with message
                confirmationWindowUI.OpenConfirmationWindow(
                    "Are you want to use "+ gunSO.gunName +" ?");
                isUse = true;
            }

            
        });

        rentOutBtn.onClick.AddListener(() =>
        {
            // Open confirm window with message
            confirmationWindowUI.OpenConfirmationWindow(
                    "Are you want to rent out " + gunSO.gunName + " ?");
            isUse = false;
        });
    }

    private void Start()
    {
        TabGroup.Instance.OnSelectTabBtn += TabGroup_OnSelectTabBtn;

        confirmationWindowUI.OnlClickYesBtn += ConfirmationWindowUI_OnlClickYesBtn;
    }

    private void ConfirmationWindowUI_OnlClickYesBtn(object sender, EventArgs e)
    {
        if (isUse)
        {
            useTxt.text = "Used";
            OnClickUseBtn?.Invoke(this, EventArgs.Empty);
        }
        else
        {
            rentTxt.text = "Rended out";
            OnClickRentOutBtn?.Invoke(this, EventArgs.Empty);
        }
        
    }

    private void TabGroup_OnSelectTabBtn(object sender, TabGroup.OnSelectTabBtnEventArgs e)
    {
        gunSO = e.gunSO;
        UpdatePrimaryDisplay(gunSO);
    }

    // Update display text for use, rent button
    void UpdatePrimaryDisplay(GunSO gunSO)
    {
        primaryWeaponImage.sprite = gunSO.gunSprite;

        if (gunSO.rentOut)
        {
            rentTxt.text = "Rended out";
        }
        else
        {
            rentTxt.text = "Rent out";
        }
        if (gunSO.use)
        {
            useTxt.text = "Used";
        }
        else
        {
            useTxt.text = "Use";
        }
    }
}

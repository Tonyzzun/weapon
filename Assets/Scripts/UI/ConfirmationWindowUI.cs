using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class ConfirmationWindowUI : MonoBehaviour
{
    public Button yesBtn, noBtn;
    public Text messageTxt;

    public event EventHandler OnlClickYesBtn;

    private void Awake()
    {
        yesBtn.onClick.AddListener(() =>
        {
            // Fire evnet on click YES
            OnlClickYesBtn?.Invoke(this, EventArgs.Empty);
            gameObject.SetActive(false);

        });

        noBtn.onClick.AddListener(() =>
        {
            gameObject.SetActive(false);

        });
    }

    // Show Confirm window
    public void OpenConfirmationWindow(string message)
    {
        gameObject.SetActive(true);
        messageTxt.text = message;
        
    }

}

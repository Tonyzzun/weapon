using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using UnityEngine.UI;

public class TabGroup : MonoBehaviour
{
    List<TabBtn> tabBtns;

    public Sprite tabIdle, tabHover, tabActive;

    TabBtn selectedTabBtn;

    [SerializeField] TabBtn gunPrefab;
    [SerializeField] GunSO[] gunSOList;

    [SerializeField] PrimaryHolderManager primaryHolderManager;

    public static TabGroup Instance { get; private set; }

    public event EventHandler<OnSelectTabBtnEventArgs> OnSelectTabBtn;
    public class OnSelectTabBtnEventArgs : EventArgs
    {
        public GunSO gunSO;
    }

    private void Awake()
    {
        if (Instance != null)
        {
            Debug.LogError("There is more than 1 Instance TabGroup");
        }
        Instance = this;

    }

    private void Start()
    {
        CreateGunMenu();
        SelectTheFirstGunWhenStart();
        primaryHolderManager.OnClickUseBtn += PrimaryHolderManager_OnClickUseBtn;
        primaryHolderManager.OnClickRentOutBtn += PrimaryHolderManager_OnClickRentOutBtn;
    }

    private void PrimaryHolderManager_OnClickUseBtn(object sender, EventArgs e)
    {
        OnClickUseBtnHandle();
    }
    private void PrimaryHolderManager_OnClickRentOutBtn(object sender, EventArgs e)
    {
        OnClickRentOutBtnHandle();
    }

    // Add a TabBtn to list
    public void Subscribe(TabBtn btn)
    {
        if (tabBtns == null)
        {
            tabBtns = new List<TabBtn>();
        }

        tabBtns.Add(btn);
    }

    // Change backGround of TabBtn when hovered
    public void OntabEnter(TabBtn btn)
    {
        ResetTabs();
        if (selectedTabBtn == null || btn != selectedTabBtn)
        {
            btn.backGround.sprite = tabHover;
        }
    }

    // Change backGround of TabBtn when Exit
    public void OntabExit()
    {
        ResetTabs();
    }

    // Change backGround of TabBtn when Selected and fire an event
    public void OntabSelected(TabBtn btn)
    {
        /*if (selectedTabBtn != null)
        {
            //selectedTabBtn.Deselect();
        }*/

        selectedTabBtn = btn;

        //selectedTabBtn.Select();

        ResetTabs();

        OnSelectTabBtn?.Invoke(this, new OnSelectTabBtnEventArgs
        {
            gunSO = btn.GetGunSO()
        });
        btn.backGround.sprite = tabActive;

    }

    // Reset backgrouc of TabBtn to idle
    public void ResetTabs()
    {
        foreach (TabBtn tabBtn in tabBtns)
        {
            if (selectedTabBtn != null && tabBtn == selectedTabBtn)
            {
                continue;
            }
            tabBtn.backGround.sprite = tabIdle;
        }
    }

    // Create Guns for gun menu
    public void CreateGunMenu()
    {
        // clear tab group before create gun menu
        while (transform.childCount>0)
        {
            DestroyImmediate(transform.GetChild(0).gameObject);
        }

        // Create guns
        foreach (GunSO item in gunSOList)
        {
            CreateGunitem(item);

        }
    }

    // Create a single gun
    void CreateGunitem(GunSO item)
    {
        TabBtn tabBtn = Instantiate(gunPrefab, gameObject.transform);
        tabBtn.tabGroup = this;
        tabBtn.gunSO = item;

        // Create status text below the gun
        if (item.use)
        {
            tabBtn.statusTxt.text = "Used";
            tabBtn.statusTxt.color = tabBtn.green;
        }
        else if (item.rentOut)
        {
            tabBtn.statusTxt.text = "Rended Out";
            tabBtn.statusTxt.color = tabBtn.orange;
        }
        else
        {
            tabBtn.statusTxt.text = null;
        }
    }

    void SelectTheFirstGunWhenStart()
    {
        selectedTabBtn = transform.GetChild(0).gameObject.GetComponent<TabBtn>();
        OnSelectTabBtn?.Invoke(this, new OnSelectTabBtnEventArgs
        {
            gunSO = selectedTabBtn.GetGunSO()
        });

        selectedTabBtn.GetComponent<Image>().sprite = tabActive;
    }

    void OnClickUseBtnHandle()
    {
        if (!selectedTabBtn.gunSO.rentOut || selectedTabBtn.gunSO.use)
        {
            return;
        }

        int index = 0;
        // change status using gun to un-use
        foreach (GunSO item in gunSOList)
        {
            if (item != selectedTabBtn.gunSO && item.use)
            {
                // update data for gun in menu
                item.use = !item.use;

                // update displplay for gun in menu
                var tabBtnTemp = transform.GetChild(index).gameObject.GetComponent<TabBtn>();
                tabBtnTemp.statusTxt.text = "Rended Out";
                tabBtnTemp.statusTxt.color = tabBtnTemp.orange;
            }
            index++;
        }

        // change status current gun to used
        // for data
        selectedTabBtn.gunSO.use = !selectedTabBtn.gunSO.use;

        //for display
        selectedTabBtn.statusTxt.text = "Used";
        selectedTabBtn.statusTxt.color = selectedTabBtn.green;
    }

    void OnClickRentOutBtnHandle()
    {
        if (selectedTabBtn.gunSO.rentOut)
        {
            return;
        }

        // change status using gun to un-use

        for(int i = 0; i < transform.childCount ; i++)
        {
            var tabBtnTemp = transform.GetChild(i).gameObject.GetComponent<TabBtn>();

            if (tabBtnTemp.gunSO == selectedTabBtn.gunSO)
            {
                // update data for gun in menu
                tabBtnTemp.gunSO.rentOut = true;

                // update displplay for gun in menu
                tabBtnTemp.statusTxt.text = "Rended Out";
                tabBtnTemp.statusTxt.color = tabBtnTemp.orange;
            }
        }
    }
}
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class GunSO : ScriptableObject
{
    public Sprite gunSprite;
    public string gunName;
    public float gunDmg, gunDispersion, gunFOR, gunReloadSpeed, gunAmmunitio, yellowNum, greenNum;
    public bool use, rentOut, upgrade;

}
